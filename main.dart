import 'package:flutter/material.dart';
import 'package:vunenerisiva_module3/homepage.dart';
import 'package:splashscreen/splashscreen.dart';

void main() {
  runApp(new MaterialApp(
          title: "Flutter",
      theme: ThemeData(
      backgroundColor: Colors.black,

      colorScheme: ColorScheme.fromSwatch(
      primarySwatch: Colors.blueGrey).copyWith(secondary:Colors.black)
      ,
      textTheme:TextTheme(),
      appBarTheme: AppBarTheme(
      backgroundColor: Colors.blueGrey,
      centerTitle: true
      )),

      home:new MyApp())
  );
}

class MyApp extends StatefulWidget{
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp>
{
  @override
  Widget build(BuildContext context)
  {
    return new SplashScreen
      (
        seconds: 14,
        navigateAfterSeconds: openpage(),
        title: new Text("MTN")
    );
  }
}


//
// title: "Flutter",
// theme: ThemeData(
// backgroundColor: Colors.black,
//
// colorScheme: ColorScheme.fromSwatch(
// primarySwatch: Colors.blueGrey).copyWith(secondary:Colors.black)
// ,
// textTheme:TextTheme(),
// appBarTheme: AppBarTheme(
// backgroundColor: Colors.blueGrey,
// centerTitle: true
// ),
// )

import 'package:flutter/material.dart';
import 'package:vunenerisiva_module3/loginpage.dart';
import 'package:vunenerisiva_module3/registrationpage.dart';

class openpage extends StatelessWidget
{
  const openpage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context)
  {
    return Scaffold
      (
      appBar:AppBar
        (
        title: Center (child: Text("Let's Get Started")),
      ),

      body: Column
        (
        mainAxisAlignment:MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children:[
        ElevatedButton(onPressed: ()=> {Navigator.push(context,
           MaterialPageRoute(builder:
               (context) =>registration()))}, child: const Text("Sign up")),
           ElevatedButton(onPressed: ()=> {Navigator.push(context,
               MaterialPageRoute(builder:
                   (context) =>loginpage()))}, child: const Text("Login")),
          Container(height:40.0,decoration:BoxDecoration(image: DecorationImage(image: AssetImage("assets/images/lady.png"))))
        ]
        ),
        bottomNavigationBar:BottomAppBar
          (
          shape: const CircularNotchedRectangle(),
        )
      );

  }
}

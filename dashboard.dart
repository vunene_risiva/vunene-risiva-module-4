import 'package:flutter/material.dart';
import 'package:vunenerisiva_module3/feature1.dart';
import 'package:vunenerisiva_module3/feature2.dart';
import 'package:vunenerisiva_module3/homepage.dart';

class dashboard extends StatelessWidget
{
  const dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context)
  {
    return Scaffold
          (
            appBar:AppBar
              (
                title: Center (child: Text("Dashboard")),
            ),

          body: Column(
              mainAxisAlignment:MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
            children:[
              ElevatedButton(onPressed: ()=> {Navigator.push(context,
                  MaterialPageRoute(builder:
                      (context) =>profile_edit()))}, child: const Text("Edit Profile")),
              ElevatedButton(onPressed: ()=> {Navigator.push(context,
                  MaterialPageRoute(builder:
                      (context) =>help()))}, child: const Text("Help")),
              ElevatedButton(onPressed: ()=> {Navigator.push(context,
                  MaterialPageRoute(builder:
                      (context) =>openpage()))}, child: const Text("Exit"))
            ]
          ),
        );
  }
}
